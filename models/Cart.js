const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
  products : [
    {
      productId : {
        type : String,
        required : [true, "Product ID is required"]
      },
      quantity : {
        type : Number,
        required : [true, "Quantity is required"] 
      }
    }
  ]
})

module.exports = mongoose.model("Cart", cartSchema);