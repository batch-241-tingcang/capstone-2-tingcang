const Product = require("../models/Products");

// Create Product (Admin only)
module.exports.createProduct = (reqBody) => {
  let newProduct = new Product({
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
    imageURL: reqBody.imageURL,
    isActive: reqBody.isActive,
  });

  return newProduct.save().then((product, error) => {
    if (error) {
      return false;
    } else {
      return true;
    }
  });
};

//Retrieve ALL products (whether available or not)
module.exports.getAllProducts = () => {
  return Product.find().then((result) => {
    return result;
  });
};

// Retrieve all ACTIVE products
module.exports.getAllActive = () => {
  return Product.find({ isActive: true }).then((result) => {
    return result;
  });
};

// Retrieve SINGLE product
module.exports.getSingleProduct = (reqparams) => {
  return Product.findById(reqparams.productId).then((result) => {
    return result;
  });
};

// Update Product information (Admin only)
module.exports.updateProduct = (reqParams, reqBody) => {
  let updateProduct = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
    imageURL: reqBody.imageURL,
  };
  return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then(
    (course, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    }
  );
};

// Archive Product (Admin only)
module.exports.toggleAvailability = (reqParams, reqBody) => {
  let updateActiveField = {
    isActive: reqBody.isActive,
  };
  return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then(
    (course, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    }
  );
};

// Delete Product (Admin only) * Additional Feature
module.exports.deleteProduct = (productId) => {
  return Product.findByIdAndRemove(productId).then((removedProduct, err) => {
    if (err) {
      console.log(err);
      return false;
    } else {
      return removedProduct;
    }
  });
};
