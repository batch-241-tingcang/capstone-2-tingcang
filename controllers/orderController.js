const Order = require("../models/Orders");
const Product = require("../models/Products");

// Non-admin User Checkout (Creating Orders)
module.exports.createOrder = async (reqBody, userId) => {
  const productIdList = reqBody.products.map((product) => product.productId);

  const result = await Product.find({
    _id: {
      $in: productIdList,
    },
  });

  let totalAmount = 0;

  for (const product of result) {
    totalAmount +=
      product.price *
      reqBody.products.find(
        (obj) => obj.productId.toString() === product._id.toString()
      ).quantity;
  }

  let newOrder = new Order({
    userId: userId,
    products: reqBody.products,
    totalAmount: totalAmount,
  });
  return newOrder.save().then((order, error) => {
    if (error) {
      return false;
    } else {
      return true;
    }
  });
};

module.exports.fetchUserOrders = async (reqBody, userId) => {
  return Order.find({ userId }).then((orders, error) => {
    if (error) {
      return false;
    }

    return orders;
  });
};
