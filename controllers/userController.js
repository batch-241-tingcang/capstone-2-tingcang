const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Orders = require("../models/Orders");

//Checking if the email exists in the database
module.exports.checkEmailExists = (reqBody) => {
  return User.find({ email: reqBody.email }).then((result) => {
    if (result.length > 0) {
      return true;
    } else {
      return false;
    }
  });
};

// User Registration
module.exports.registerUser = (reqBody) => {
  return User.find({ email: reqBody.email }).then((result) => {
    if (result.length > 0) {
      return { message: "Duplicate email found", success: false };
    } else {
      let newUser = new User({
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
      });

      return newUser.save().then((user, error) => {
        if (error) {
          return { message: "An error occurred", success: false };
        } else {
          return {
            message: "Successfully registered",
            data: user,
            success: true,
          };
        }
      });
    }
  });
};

// User Authentication
module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    console.log(result);

    if (result == null) {
      return false;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );

      if (isPasswordCorrect) {
        return { access: auth.createAccessToken(result) };
      } else {
        return false;
      }
    }
  });
};

// Retrieve User Details
module.exports.getUserDetails = (userId) => {
  return User.findById(userId).then((result) => {
    return Orders.find({ userId: userId }).then((orderItems) => {
      return {
        email: result.email,
        isAdmin: result.isAdmin,
        orders: orderItems,
      };
    });
  });
};
