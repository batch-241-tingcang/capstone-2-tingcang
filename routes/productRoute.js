const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// Route for creating a product (Admin only)
router.post("/createProduct", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  if (!isAdmin) {
    return res.status(401).send("Unauthorized");
  }
  productController
    .createProduct(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Route for retrieving ALL products (whether available or not)
router.get("/all", (req, res) => {
  productController
    .getAllProducts()
    .then((resultFromController) => res.send(resultFromController));
});

// Route for retrieving all ACTIVE products
router.get("/active", (req, res) => {
  productController
    .getAllActive()
    .then((resultFromController) => res.send(resultFromController));
});

// Route for retrieving SINGLE product
router.get("/:productId", (req, res) => {
  productController
    .getSingleProduct(req.params)
    .then((resultFromController) => res.send(resultFromController));
});

// Route for Update Product information (Admin only)
router.put("/:productId", auth.verify, (req, res) => {
  productController
    .updateProduct(req.params, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Route for Archiving a Product (Admin only)
router.patch("/:productId/toggleAvailability", auth.verify, (req, res) => {
  productController
    .toggleAvailability(req.params, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Route for Deleting a Product (Admin only) * Additional Feature
router.delete("/:productId", auth.verify, (req, res) => {
  productController
    .deleteProduct(req.params.productId)
    .then((resultFromController) => res.send(resultFromController));
});

// Route for Non-admin User checkout (Create Order)

module.exports = router;
