const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

//Route for checking if the user's email already exists in our database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for User Registration
router.post("/register", (req, res) => {
  userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for User Authentication
router.post("/login", (req, res) => {
  userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for Retrieving User Details
router.get("/my-profile", (req, res) => {
  const userId = auth.decode(req.headers.authorization).id
  userController.getUserDetails(userId).then(resultFromController => res.send(resultFromController));
});

module.exports = router;