const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../auth");

// Route for Non-admin User Checkout (Creating Orders)
router.post("/checkout", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  if (isAdmin) {
    return res.status(401).send("Unauthorized");
  }

  const userId = auth.decode(req.headers.authorization).id;
  orderController
    .createOrder(req.body, userId)
    .then((resultFromController) => res.send(resultFromController));
});

router.get("/all", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  if (isAdmin) {
    return res.status(401).send("Unauthorized");
  }

  const userId = auth.decode(req.headers.authorization).id;
  orderController
    .fetchUserOrders(req.body, userId)
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
