const express = require("express");
const app = express();
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const port = 4000;
const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute.js");
const orderRoute = require("./routes/orderRoute.js");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

mongoose.set("strictQuery", false);
mongoose.connect(
  "mongodb+srv://admin123:admin123@cluster0.o0hvxs8.mongodb.net/Capstone-2?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the cloud database"));

app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute);

app.listen(port, () => {
  console.log(`API is now online on port ${port}`);
});
